# Week 06: Draft Final Report

#### _2021-spring_
#### _`date_scheduled = datetime(2021, 5, 7, 20, 30, 0)`_
#### _`title = 'broadcast'`_

You pushed a draft report for you internship project as soon as you started thinking of ideas, right? ;)  If you haven't already, create a fork of the `tangibleai/team/`[^1] repository on GitLab. Then find the learning-resources/projects directory and add a markdown file (text file with the `.md` extension) to hold your draft final report. Make sure it has your problem statement or a summary of what you plan to accomplish in an "introduction" or "abstract" section at the top of the markdown file. 

## DS, ML, and DL Projects

For data science projects, create sections called `## ETL`, `## EDA`, and a `## Model Tuning`. If you don't know what those sections are about, check out last quarters' reports here: https://proai.org/intern-project-reports .  

## Other Projects

For other software development projects like chatbots, the qary platform, conversation design, or web applications (Django), your report should include sections for each of the major features you developed into your app or chatbot. You want to describe a typical use case in a user story and share how you architected the software to make it happen (the APIs for any classes, functions, web pages, or databases you created). If you need ideas, check out the previous intern reports or ask about your project on the #interns slack channel.

[^1:] https://proai.org/gitlab-team-learning-resouces or gitlab.com/tangibleai/team/  
[^2:] https://proai.org/intern-project-reports or gitlab.com/tangibleai/team/-/blob/master/learning-resources/projects/reports/README.md#final-reports
