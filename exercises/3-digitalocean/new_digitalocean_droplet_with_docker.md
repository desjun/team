
# Steps to create a DigitalOcean droplet with Docker installed

I followed this tutorial https://testdriven.io/blog/deploying-django-to-digitalocean-with-docker-and-gitlab/
Note: the first few commands are slightly different than what is in the tutorial but would not work otherwise
##### change name
```bash
$ curl -X POST \
     -H 'Content-Type: application/json' \
     -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
     -d '{"name":"tangible-rasa","region":"sfo2","size":"s-2vcpu-4gb","image":"docker-20-04"}' \
     "https://api.digitalocean.com/v2/droplets"
```
##### change name
```bash
$ curl \
     -H 'Content-Type: application/json' \
     -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
     "https://api.digitalocean.com/v2/droplets?name=tangible-rasa" \
   | jq '.droplets[0].status'
```

### log in with password emailed to you
```bash
$ ssh root@<YOUR_INSTANCE_IP>
```
change password when promted

### Next, generate a new SSH key
```bash
$ ssh-keygen -t rsa
```

####Enter file in which to save the key (/root/.ssh/id_rsa):
enter or
```bash
$ /root/.ssh/id_rsa
```

#### Enter passphrase (empty for no passphrase):
Enter or add a passphrase


## To set up passwordless SSH login, copy the public key over to the authorized_keys file and set the proper permissions:

root@tangible-rasa:~#
```bash
$ cat ~/.ssh/id_rsa.pub
$ nano ~/.ssh/authorized_keys
$ chmod 600 ~/.ssh/authorized_keys
$ chmod 600 ~/.ssh/id_rsa
```

### Copy the contents of the private key:
```bash
$ cat ~/.ssh/id_rsa
```

#### Set it as an environment variable on your local machine:
```bash
$ export TANGIBLE_RASA_PRIVATE_KEY='
-----BEGIN OPENSSH PRIVATE KEY-----
23bsdcf3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEb...........
......................................................
...............==
-----END OPENSSH PRIVATE KEY-----'
```

### Add the key to the ssh-agent:
```bash
$ ssh-add <<< "${TANGIBLE_RASA_PRIVATE_KEY}"
```

### To test, run:
```bash
$ ssh -o StrictHostKeyChecking=no root@<YOUR_INSTANCE_IP> whoami  # --> root
```
