# Get Started with Git

## Beginner

It's best if you follow these instructions as closely as possible, without taking any shortcuts.
For example typing the URLs in your browser will prevent search engines (including GitLabs) from deistracting you from your task.
And it will help you start to recognize the form of URLs and directory names that you will eventually need to type on the command line.

1. Create a free account on GitLab.com
2. Browse Tangible AI's repository of community resources named `team` by typing the following URL into your browser's address bar (not search) `gitlab.com/tangibleai.com`
3. Dismiss any nag banners at the top of the page so you don't get distracted.
4. Click the `Fork` button to create a copy of the Tangible AI `team` repository and select your GitLab account name as the "project" to fork/copy it to. 
5. Select "public" on the repository visibility radio buttons so that other interns can see it.
6. Browse to your fork of the `team` repository in your personal account: `gitlab.com/{your_gitlab_username}/team`
7. Type the URL of your Fork of our `team` repository.
8. Explore around in the repository, looking for exercises or resources that interest you.
9. Next to the pull-down menu with the branch name `main` notice there's a path where you are currently located with the names of directories separated by forward slashes (`/`).
10. Find the `reports` directory inside the `project-reports` directory within your fork of the `team` repository.
11. Within the `reports` directory click on the + sign to the right of the `reports` directory and select "create new file".
12. In the text box at the top name your file `{year}-{your_first_name}-{notes}.md` for example `2022-hobs-notes.md`..
13. Create a heading in the new markdown (`.md`) file with something like `# {your name} Notes`.
14. Type a summary sentence or two describing how you will keep notes here each week to keep track of your progress on the Tangible AI internship.
15. Commit your new file with the button in the lower left of the screen (or if your already created the file and are editing using the web IDE click create commit and select the commit to main branch option).
16. Join the #qary channel on the San Diego Machine Learning group.
17. Send a message to the #qary channel telling us your username and where we can find the notes file that you just created on GitLab (the URL for your file).

 ## Bonus

 Follow the beginner steps above, then:

18. Browse to Tangible AI's copy of the `team` repository.
19. Use the banner nag prompt at the top, or the side bar or lefthand "hamburger" menu, to create a Merge request.
20. You want to request that I merge your changes to the `team` repository into the `main` branch of our repository at Tangible AI.
21. Submit the merge request.
22. Find the merge request that you just created.
23. In the browser address bar copy the URL for the merge request that you created.
24. Slack me a link to your merge request.

 ## Intermediate

 Follow the 23 steps of the [Beginner](#beginner) and [Bonus](#bonus) sections above.

25. Find the `README.md` for this exercise in your Fork of the `team` respository!
26. Use the `Edit in IDE` button on the page for this `README.md` file.
27. Correct any of the steps in the [Beginner](#beginner) section that were unclear to you. OR ...
28. Add a new step or suggested activity to the [Beginner](#beginner), [Intermediate](#intermediate), or [Advanced](#advanced) sections.
29. Commit your changes
30. Create a merge request for this file

## Advanced

Accomplish all of the above steps, but do as much of it as possible using your developer toolbox:

* a terminal with a bash shell and `git` installed (`git-bash`, `bash`, or `zsh`)
* an IDE (Sublime Text or [VSCodium](https://vscodium.com/))

Some of the steps must still be done with the browser or another application GUI.

* Steps 1-4 in the [beginner](#beginner) section
* The merge request tasks in the [Bonus](#bonus) and [Intermediate](#intermediate) sections.
