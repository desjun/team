# Dirty Data

Once you've [found a dataset you like](./datasets.md), the next step is to download it and clean it up a little.
If you're lucky Pandas may be able to do most of what you need automatically.

# Lesson Plan 
For todays assignment we will be using a mlb_players.csv dataset and learning common preconceptions and issues related to pd.read_csv. We will start with finding the limitations of read_csv before utilizing curl and our EDA skillset to determine a resolution utilizing .strip() that will lead to a cleaner dataset. 

## Pandas Problems

```python
>>> import pandas as pd
>>> df = pd.read_csv(
...    'https://people.sc.fsu.edu/~jburkardt/data/csv/mlb_players.csv')                                                     
---------------------------------------------------------------------------
ParserError                               Traceback (most recent call last)
<ipython-input-31-eed980048a97> in <module>
----> 1 df = pd.read_csv('https://people.sc.fsu.edu/~jburkardt/data/csv/mlb_players.csv')
...

pandas/_libs/parsers.pyx in pandas._libs.parsers.raise_parser_error()

ParserError: Error tokenizing data. C error: EOF inside string starting at row 1035
```

As usual, pay close attention to the last line of the traceback (error message log). 
You want to pay close attention to the first line of the error message (to make sure it's your line of code).

Next lets try to use the bash curl command within the iPython REPL (console). 
An exclamation mark (`!`) tells the ipython interpreter to execute your command in `bash` rather than the python interpretter. 

## `curl`
```python
>>> !curl 'https://people.sc.fsu.edu/~jburkardt/data/csv/mlb_players.csv'
"Name", "Team", "Position", "Height(inches)", "Weight(lbs)", "Age"
"Adam Donachie", "BAL", "Catcher", 74, 180, 22.99
...
"Josh Kinney", "STL", "Relief Pitcher", 73, 195, 27.92
>>> ls *.csv
>>> ls -al *.csv
```

Notice that you didn't have to use the exclamation mark to tell ipython about the `ls` command?
There are a few bash commands that ipython knows "by heart", such as `ls`, `more`, and cd.
And you used the options `-a` and `-l` to make absolutely sure there are no csv files in your current working directory.

So that ls (list directory) command tells us that our download didn't work with `curl`.
There are no csv files in our ls.

It just reads the remote file into "stdout" which streams it to your terminal.
To stream it to a file you can use the `>` output redirection operator in bash.
But I prefer the `>>` which appends to the destination file/stream, in case there already is a file with the name I specify: 

#### `curl >`
```python
>>> !curl https://people.sc.fsu.edu/~jburkardt/data/csv/mlb_players.csv > mlb_players.csv
```

#### `curl -O`
```python
>>> !curl -O https://people.sc.fsu.edu/~jburkardt/data/csv/mlb_players.csv
```

## Bash variables 

What if you stored that long URL in a python variable.
Do you think the  `ipython` developers were generous enough to give you a way to use that variable within `bash`?
What's the syntax for variables in `bash`?
Can you write a command that will display the contents of a python variable within `bash`.

Here's a hint:

```python
>>> !USER=jesse
>>> !printf "${USER}"
jesse
>>> !USER_LAST="owens"
>>> !echo "$USER $USER_LAST"
jesse owens
```

Now how do you get a variable defined in python into bash?
And could you do it the other way around?
Could you take an environment variable like $PATH or $USER and `print()` it in python?

If you figure out how to move data from a python variable to a bash variable in python, you could create a `url` variable to use for both `curl` **and** `pd.read_csv`.

If you don't like the way curl works, the `wget` command will work very similarly to `curl` but may be a bit easier. 
Type `!man wget` or `!wget --help` for more information.

## Data Cleaning

```python
>>> more mlb_players.csv                             
"Name", "Team", "Position", "Height(inches)", "Weight(lbs)", "Age"
"Adam Donachie", "BAL", "Catcher", 74, 180, 22.99
...
"Steve Trachsel", "BAL", "Starting Pitcher", 76, 205, 36.33
```

That's better. At least we now have the file on our computer.
Let's try to read it in line by line with python to see why Pandas can't parse it with `read_csv()`.

```python
>>> rows = [] 
... filestream = open('mlb_players.csv')
... for line in filestream:
...     rows.append(line.split())
>>> rows[:2]
... [['"Name",',
...   '"Team",',
...   '"Position",',
...   '"Height(inches)",',
...   '"Weight(lbs)",',
...   '"Age"'],
...  ['"Adam', 'Donachie",', '"BAL",', '"Catcher",', '74,', '180,', '22.99']]
```

Oh, that explains it.
There's something weird about both the header row and the first row of data.
You could skip the header, which seems to contain some ugly carriage-return (`'\r'`) rubbish from the Microsoft-vs-Mac code wars.
But who knows what else is wrong with the file, like that first-name last name split with a newline (`'\n'`) or some other invisible character.

If you do the [Duck](https://duckduckgo.com/?t=canonical&q=python+read+file+EOF+inside+string&ia=web) you'll find that `pandas.read_csv()` has a lot of options that might help you.

You could try `encoding='latin'` in case Microsoft nonstandard encoding was the only problem.
But that won't do it.
Those newlines or EOF characters within the quotes are probably the main problem.

```python
>>> df = pd.read_csv('mlb_players.csv', encoding='latin')
...
ParserError: Error tokenizing data. C error: EOF inside string starting at row 1035
```

Same old, same old.

And skiprows also fails:

```python
>>> df = pd.read_csv('mlb_players.csv', skiprows=1)
...
ParserError: Error tokenizing data. C error: EOF inside string starting at row 1035
```

So if you you'd thought hard about the error message from the beginning, and maybe RTMed.

```python
>>> help(pd.read_csv)
```

You can see that there's an option to deal with those pesky quote characters.

Try ignoring the quote characters and just read them in as part of the string. 
You can strip of the quotes, and all those odd invisible (whitespace) characters later, during data cleaning.


```python
>>> import csv
>>> df = pd.read_csv('mlb_players.csv', quoting=csv.QUOTE_NONE)
>>> df
                 "Name"  "Team"  ...  "Weight(lbs)"   "Age"
0       "Adam Donachie"   "BAL"  ...            180   22.99
1           "Paul Bako"   "BAL"  ...            215   34.69
2     "Ramon Hernandez"   "BAL"  ...            210   30.78
3        "Kevin Millar"   "BAL"  ...            210   35.43
4         "Chris Gomez"   "BAL"  ...            188   35.71
...                 ...     ...  ...            ...     ...
1030    "Tyler Johnson"   "STL"  ...            180   25.73
1031   "Chris Narveson"   "STL"  ...            205   25.19
1032    "Randy Keisler"   "STL"  ...            190   31.01
1033      "Josh Kinney"   "STL"  ...            195   27.92
1034                  "     NaN  ...            NaN     NaN
[1035 rows x 6 columns]
```

Worked like a charm. 
So now all you have to do is get rid of that last dirty row and those pesky quotes.

You probably want to clean up the column labels first.
That way you can easily access individual columns during the cleaning:

```python
>>> df.columns = [c.strip().strip('"').strip().lower() for c in df.columns]
>>> df.columns = [c.strip(')').replace('(', '_') for c in df.columns]
>>> df.columns
Index(['name', 'team', 'position', 'height_inches', 'weight_lbs', 'age'],
      dtype='object')
```

Notice that `df.columns` is a `pandas.Index`?
That's a special kind of `pandas.Series`. 
It is like the difference between a dictionary and a list.
A `pandas.Index` can contain nonnumerical objects, like strings.
That makes it easier to label a column with something meaningful.

## Explore data using statistics

The first thing you want to do with a new dataset is to display some descriptive statistics.
You can display statistics such as the minimum and maximum values the numerical columns with `.describe()`:

```python
>>> df.describe()
       height_inches          age
count    1034.000000  1034.000000
mean       73.697292    28.736712
std         2.305818     4.320310
min        67.000000    20.900000
25%        72.000000    25.440000
50%        74.000000    27.925000
75%        75.000000    31.232500
max        83.000000    48.520000
```

The `.describe()` method even gives you the edges of a box plot.
This is kind of like a variable-bin-width 4-bin histogram, where the bars get wider rather than taller to represent more data.
It gives you the values at each of the 4 quartile boundaries: the smallest value, the largest of the 25% smallest values, the largest of the 50% largets values (median), and so on up to the max value.

But only 2 columns show up as having numerical data. 
So you want to display statistics (counts) on the nonnumerical (string) data too:

```python
>>> df.describe(include='all')
               name    team           position  height_inches weight_lbs          age
count          1035    1034               1034    1034.000000       1035  1034.000000
unique         1033      30                  9            NaN         91          NaN
top     "Tony Pe?a"   "NYM"   "Relief Pitcher"            NaN        200          NaN
freq              2      38                315            NaN        108          NaN
mean            NaN     NaN                NaN      73.697292        NaN    28.736712
std             NaN     NaN                NaN       2.305818        NaN     4.320310
min             NaN     NaN                NaN      67.000000        NaN    20.900000
25%             NaN     NaN                NaN      72.000000        NaN    25.440000
50%             NaN     NaN                NaN      74.000000        NaN    27.925000
75%             NaN     NaN                NaN      75.000000        NaN    31.232500
max             NaN     NaN                NaN      83.000000        NaN    48.520000
```

## `NaN`s

IEEE set up a way to keep track of unusually big and small values or missing values within the bits and bytes of a computer.
There are several of these defined within `numpy`:

- `inf`: *Infinity*: a number too big to fit in the allowed bits (usually 64)
- `nan`: *Not a number*: a null or number too small to be represented 
- `-inf`: *Negative infinity*: a number too negative to be represented

You can get the python numerical float for these crazy numbers by coercing a string into a float:

```python
>>> float('inf')
inf
>>> float('-inf')
-inf
>>> float('nan')
nan
```

And you can find them as constants within the numpy package:

```python
>>> np.nan
nan
>>> np.inf
inf
>>> -np.inf
-inf
```

These are designed to handle math in a repeatable way on any computer, in any python package, anywhere in the world, no matter what your government tells you is the truth.

How do you think humans around the world deal with this calculation: `-np.nan`. Or what about `1 + np.nan`.
Here are some more to play with.
You'll want to explore around the limits of the number line yourself.
There's some crazy stuff that happens at the edge of the mathematics universe.

If `np.nan` and `np.inf` were your only options for representing crazy big, crazy small, and just plain missing values, how do you think you would represent the results of these expressions.

What happens when you negate or invert infinity?
What happens when you add or multiply infinities?

```python
>>> -np.nan
>>> 1 + np.nan
>>> np.nan * 100000
>>> np.inf - 100000
>>> -np.inf + np.inf
>>> np.inf - np.inf
>>> 1 / 0
>>> 1 / np.inf
>>> np.inf
```

Crazy `NaN` values come up a lot in Data Science and really anything where computers and math are involved.
Be very very careful with NaNs

You can find out how many nans are in each column with some `isna().sum()` magic. 

The `isna()` part creates an array of `False`s with the same number of values (rows) that are in that column.
That way you can use it as a numpy mask within square brackets (`df[mask]`) to filter out all the NaN rows.
But first we just want to count them.
When python adds a `bool` (`True` or `False`) to another `bool`, it returns an integer:

```python
>>> False + False
0
>>> False + True
1
>>> True + False + True + True + False
3
```

That gives you an easy way to count up the `True`s in all the columns and list the count right next to each column name:

```python
>>> df.isnull().sum()
name             0
team             1
position         1
height_inches    1
weight_lbs       0
age              1
dtype: int64
```

It doesn't look like NaNs are a problem for the `weight_lbs` column.
Perhaps the last row is the problem:

```python
>>> df.iloc[-1]
name               "
team             NaN
position         NaN
height_inches    NaN
weight_lbs       nan
age              NaN
Name: 1034, dtype: object
```

Yup.
So take care of it:

```python
>>> mask_rows = df['team'].notna()
>>> df = df[mask_rows].copy()
```

That `.copy()` trick will keep Pandas from warning you about possible mistakes in manipulating your data.

## `str` accessor method

```python
Now you can start cleaning up the quotes within the data.

```python
>>> df.head(2)
              name    team    position  height_inches weight_lbs    age
0  "Adam Donachie"   "BAL"   "Catcher"           74.0        180  22.99
1      "Paul Bako"   "BAL"   "Catcher"           74.0        215  34.69
```

You can remove the quotes and other odd characters using that same strip approach. 
But if you *vectorize* the operation by doing it on an entire column at once, it will be crazy fast.
Plus it's just less code.
This is especially important for large datasets.

Here's how it works on numerical arrays, like that weight column.
There are 2.205 pounds in every kilogram:

```python
>>> df['weight_kg'] = df['weight_lbs'].astype(float) / 2.205
ValueError: could not convert string to float: ' ""'
```

Oops.
There's some garbage in the `weight_lbs` column.
You can clean that up the hard slow way, with a for loop:

```python
>>> df['weight_lbs'] = [str(w).strip().strip('"').strip() for w in df['weight_lbs']]
>>> df['weight_kg'] = df['weight_lbs'].astype(float) / 2.205
ValueError: could not convert string to float: 
```

So we may have to use a for loop after all.
We'll need fo find the pesky values and get rid of them.

```python
>>> df['weight_lbs'] = pd.to_numeric(df['weight_lbs'])
>>> df.head(3)
```
```python
>>> df['weight_kgs'] = df['weight_lbs'] / 2.205
>>> df['weight_kgs'].head(2)
```

```
You don' can vectorize the units conversion from inches to meters
>>> df                        
                   name    team           position  height_inches  weight_lbs    age
0       "Adam Donachie"   "BAL"          "Catcher"            74.0         180  22.99
1           "Paul Bako"   "BAL"          "Catcher"            74.0         215  34.69
2     "Ramon Hernandez"   "BAL"          "Catcher"            72.0         210  30.78
3        "Kevin Millar"   "BAL"    "First Baseman"            72.0         210  35.43
4         "Chris Gomez"   "BAL"    "First Baseman"            73.0         188  35.71
...                 ...     ...                ...             ...         ...    ...
1030    "Tyler Johnson"   "STL"   "Relief Pitcher"            74.0         180  25.73
1031   "Chris Narveson"   "STL"   "Relief Pitcher"            75.0         205  25.19
1032    "Randy Keisler"   "STL"   "Relief Pitcher"            75.0         190  31.01
1033      "Josh Kinney"   "STL"   "Relief Pitcher"            73.0         195  27.92
1034                  "     NaN                NaN             NaN         NaN    NaN

[1035 rows x 6 columns]

In [25]: df.columns = [c.strip().strip('"').strip().lower().replace(')', '').replace('(', '_') for c in df.columns]                            

In [26]: df.head()                                                                                                                             
Out[26]: 
                name    team          position  height_inches_ weight_lbs_    age
0    "Adam Donachie"   "BAL"         "Catcher"            74.0         180  22.99
1        "Paul Bako"   "BAL"         "Catcher"            74.0         215  34.69
2  "Ramon Hernandez"   "BAL"         "Catcher"            72.0         210  30.78
3     "Kevin Millar"   "BAL"   "First Baseman"            72.0         210  35.43
4      "Chris Gomez"   "BAL"   "First Baseman"            73.0         188  35.71

In [27]: df.columns = [c.strip('_') for c in df.columns]                                                                                       

In [28]: history -o -p                                                                                                                         
>>> import pandas as pd
>>> pd.read_csv('https://people.sc.fsu.edu/~jburkardt/data/csv/biostats.csv')
    Name       "Sex"   "Age"   "Height (in)"   "Weight (lbs)"
0   Alex         "M"      41              74              170
1   Bert         "M"      42              68              166
2   Carl         "M"      32              70              155
3   Dave         "M"      39              72              167
4   Elly         "F"      30              66              124
5   Fran         "F"      33              66              115
6   Gwen         "F"      26              64              121
7   Hank         "M"      30              71              158
8   Ivan         "M"      53              72              175
9   Jake         "M"      32              69              143
10  Kate         "F"      47              69              139
11  Luke         "M"      34              72              163
12  Myra         "F"      23              62               98
13  Neil         "M"      36              75              160
14  Omar         "M"      38              70              145
15  Page         "F"      31              67              135
16  Quin         "M"      29              71              176
17  Ruth         "F"      28              65              131
>>> pd.read_csv('https://people.sc.fsu.edu/~jburkardt/data/csv/cities.csv')
     LatD   "LatM"   "LatS"  "NS"  ...   "LonS"   "EW"              "City"  "State"
0      41        5       59   "N"  ...        0    "W"        "Youngstown"       OH
1      42       52       48   "N"  ...       23    "W"           "Yankton"       SD
2      46       35       59   "N"  ...       36    "W"            "Yakima"       WA
3      42       16       12   "N"  ...        0    "W"         "Worcester"       MA
4      43       37       48   "N"  ...       11    "W"   "Wisconsin Dells"       WI
..    ...      ...      ...   ...  ...      ...    ...                 ...      ...
123    39       31       12   "N"  ...       35    "W"              "Reno"       NV
124    50       25       11   "N"  ...        0    "W"            "Regina"       SA
125    40       10       48   "N"  ...       23    "W"         "Red Bluff"       CA
126    40       19       48   "N"  ...       48    "W"           "Reading"       PA
127    41        9       35   "N"  ...       23    "W"           "Ravenna"      OH 

[128 rows x 10 columns]
>>> pd.read_csv('https://people.sc.fsu.edu/~jburkardt/data/csv/hw_25000.csv')
       Index   "Height(Inches)"   "Weight(Pounds)"
0          1           65.78331           112.9925
1          2           71.51521           136.4873
2          3           69.39874           153.0269
3          4           68.21660           142.3354
4          5           67.78781           144.2971
...      ...                ...                ...
24995  24996           69.50215           118.0312
24996  24997           64.54826           120.1932
24997  24998           64.69855           118.2655
24998  24999           67.52918           132.2682
24999  25000           68.87761           124.8742

[25000 rows x 3 columns]
>>> df_mlb = pd.read_csv('https://people.sc.fsu.edu/~jburkardt/data/csv/mlb_players.csv')
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/*.csv')
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv')
>>> more '/home/hobs/Downloads/mlb_players.csv'
>>> dfmlb = []
... with open('/home/hobs/Downloads/mlb_players.csv') as fin:
...     dfmlb += [x.strip().strip(',').strip('"').strip() for x in fin.readline().split()]
...
>>> dfmlb
['Name', 'Team', 'Position', 'Height(inches)', 'Weight(lbs)', 'Age']
>>> 
... columns = dfmlb
...
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv', skiprows=0)
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv', skiprows=1)
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv', quoting=csv.QUOTE_NONE, encoding='utf-8')
>>> import csv
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv', quoting=csv.QUOTE_NONE, encoding='utf-8')
>>> dfmlb
                 "Name"  "Team"  ...  "Weight(lbs)"   "Age"
0       "Adam Donachie"   "BAL"  ...            180   22.99
1           "Paul Bako"   "BAL"  ...            215   34.69
2     "Ramon Hernandez"   "BAL"  ...            210   30.78
3        "Kevin Millar"   "BAL"  ...            210   35.43
4         "Chris Gomez"   "BAL"  ...            188   35.71
...                 ...     ...  ...            ...     ...
1030    "Tyler Johnson"   "STL"  ...            180   25.73
1031   "Chris Narveson"   "STL"  ...            205   25.19
1032    "Randy Keisler"   "STL"  ...            190   31.01
1033      "Josh Kinney"   "STL"  ...            195   27.92
1034                  "     NaN  ...            NaN     NaN

[1035 rows x 6 columns]
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv', quoting=csv.QUOTE_NONE)
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv', quoting=pd.csv.QUOTE_NONE)
>>> dfmlb = pd.read_csv('/home/hobs/Downloads/mlb_players.csv', quoting=pd.QUOTE_NONE)
>>> dfmlb
                 "Name"  "Team"         "Position"   "Height(inches)"  "Weight(lbs)"   "Age"
0       "Adam Donachie"   "BAL"          "Catcher"               74.0            180   22.99
1           "Paul Bako"   "BAL"          "Catcher"               74.0            215   34.69
2     "Ramon Hernandez"   "BAL"          "Catcher"               72.0            210   30.78
3        "Kevin Millar"   "BAL"    "First Baseman"               72.0            210   35.43
4         "Chris Gomez"   "BAL"    "First Baseman"               73.0            188   35.71
...                 ...     ...                ...                ...            ...     ...
1030    "Tyler Johnson"   "STL"   "Relief Pitcher"               74.0            180   25.73
1031   "Chris Narveson"   "STL"   "Relief Pitcher"               75.0            205   25.19
1032    "Randy Keisler"   "STL"   "Relief Pitcher"               75.0            190   31.01
1033      "Josh Kinney"   "STL"   "Relief Pitcher"               73.0            195   27.92
1034                  "     NaN                NaN                NaN            NaN     NaN

[1035 rows x 6 columns]
>>> df = dfmlb
>>> df.columns = [c.strip().strip('"').strip().lower().replace(')', '_').replace('(', '_') for c in df.columns]
>>> df
                   name    team           position  height_inches_ weight_lbs_    age
0       "Adam Donachie"   "BAL"          "Catcher"            74.0         180  22.99
1           "Paul Bako"   "BAL"          "Catcher"            74.0         215  34.69
2     "Ramon Hernandez"   "BAL"          "Catcher"            72.0         210  30.78
3        "Kevin Millar"   "BAL"    "First Baseman"            72.0         210  35.43
4         "Chris Gomez"   "BAL"    "First Baseman"            73.0         188  35.71
...                 ...     ...                ...             ...         ...    ...
1030    "Tyler Johnson"   "STL"   "Relief Pitcher"            74.0         180  25.73
1031   "Chris Narveson"   "STL"   "Relief Pitcher"            75.0         205  25.19
1032    "Randy Keisler"   "STL"   "Relief Pitcher"            75.0         190  31.01
1033      "Josh Kinney"   "STL"   "Relief Pitcher"            73.0         195  27.92
1034                  "     NaN                NaN             NaN         NaN    NaN

[1035 rows x 6 columns]
>>> df.columns = [c.strip().strip('"').strip().lower().replace(')', '').replace('(', '_') for c in df.columns]
>>> df.head()
                name    team          position  height_inches_ weight_lbs_    age
0    "Adam Donachie"   "BAL"         "Catcher"            74.0         180  22.99
1        "Paul Bako"   "BAL"         "Catcher"            74.0         215  34.69
2  "Ramon Hernandez"   "BAL"         "Catcher"            72.0         210  30.78
3     "Kevin Millar"   "BAL"   "First Baseman"            72.0         210  35.43
4      "Chris Gomez"   "BAL"   "First Baseman"            73.0         188  35.71
>>> df.columns = [c.strip('_') for c in df.columns]

```
]]
## References

- [File format Examples](https://people.sc.fsu.edu/~jburkardt/data/data.html) by John Burkardt, FSU
- [dirty CSV files](https://people.sc.fsu.edu/~jburkardt/data/csv/csv.html) by John Burkardt, FSU

