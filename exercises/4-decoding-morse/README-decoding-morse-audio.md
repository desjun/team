# Decoding Morse Audio

This week you'll learn about convolution and converting audio into text.
Don't worry, you're not going to build a complicated speech-to-text algorithm.
Instead you are going to focus on a simpler kind of "speech", Morse code.
And we've done a lot of work to get you started.

This [Jupyter notebook](https://gitlab.com/tangibleai/nlpia2/-/blob/main/src/nlpia2/ch07/decoding_morse.ipynb) will show you how to load an audio file and create and convolutional filter (kernel) that can detect the dots and dashes of morse code.

## Get started


#### 1. Clone the NLPiA2 repository

`git clone git@gitlab.com:tangibleai/nlpia2`

Or just download it using your browser:

https://gitlab.com/tangibleai/nlpia2/-/blob/main/src/nlpia2/ch07/decoding_morse.ipynb

#### 2. Open the notebook and poke around

Launch `jupyter notebook` and open the notebook you just download.
If you cloned the repo it will be at `src/nlpia2/ch07/decoding_morse.ipynb`.
Poke around to see if you can figure out what's going on.
Run some of the cells and see if you can fix any errors that pop up.
For example you may have to manually download the audio file mentioned in the notebook.
Alternatively you can install the `nlpia2` package so it can automatically run the `maybe_download` function to automatically download it.

#### 3. Watch the video(s)

Check out the twitch session where Maria and Leo and I discussed convolution and how it all works.

[May 25, 2022 twitch for manning publications](https://www.twitch.tv/videos/1492469623)

See if you can find the San Diego Python User Group lightening talk video about this exercise and watch that one too.

#### 4. Count the dots

Write some code to process the `is_dot` time series in the DataFrame and detect the occurrences of dot symbols (high probability in the `is_dot` time series).
It's not just enough to do `df['is_dot'] > .9` because you'll get a lot of `True`s in a row for each dot.
Instead you'll have to probably write a for loop that consolidates all those `True`s into a single value of 1 or `True` or even better yet `"dot"` or `"."`.

Create a new column in the dataframe to hold your `is_dot_symbol`. The column should contain 0, False or `NaN` for everything except the dot symbols. 

#### 5. Count the dashes

Write some code to process the `is_dash` time series in the DataFrame and detect the occurrences of dash symbols (high probability in the `is_dash` time series).
You'll have the same challenges as with the `is_dot` detector above.
Create a new column to hold the dashes.

#### 6. Detect pauses

Now see if you can hand-craft your own kernel to detect the pauses between "words".
You can see an example of one of these pauses right after the first 3 dots at the beginning of the file.

#### 7. BONUS

Run your code to detect dots, dashes and spaces on the entire audio file rather than just the portion that was shown in the notebook.
Now consolidate your three `is_dot_symbol`, `is_dash_symbol`, and `is_pause_symbol` columns so that you have three different symbols all in one column and `NaN`s if necessary in between.
Then do a `.dropna()` on this new column to create a series containing a sequence of three tokens: `"."`, `"-"`, and `" "` (or `"dot"`, `"dash"`, `"pause"`) based on the audio file.
Finally, write all those symbols to a text file.

Can you translate those dots and dashes into English text?
