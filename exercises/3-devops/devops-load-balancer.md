elastic load balancer
traffic
fargate
ecs
public subnets
cloud formation
cloud watch
application load balancer (layer 3 or 4 traffic, TCP)
network load balancer (don't work well for fargate and EKS) but they are faster for those ISO layers 

ecs.yml - example
vpc.yml


https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html

Jeremy and Keegan working on a the knowledge graph
https://web.stanford.edu/class/cs520/2020/notes/What_is_a_Knowledge_Graph.html


## QUANTUSflow Software GmbH Stack

- GitHub
    - Markdown (docs)
    - GitHub pages from markdown (GettingStarted and HowTo's)
    - Collaborative code review
    - Version control
    - Issue/feature tracking
    - SourceTree (git GUI)
- Visual Studio Code (IDE)
- CircleCI (CI-CD)
- Prettier / TSLint / ESLint (linting)
- SonarQube (styleguide, code health, linting)
- Docker/Docker-Compose (container management)
- VirtualBox (platform simulation)
- Kubernetes (cluster management)
- Heroku (test environments)
- nginx (facade web server for prod)
- SSLMate+OpenSSL (SSL certs)
- Amazon EC2 + S3 (staging servers)
- PostgreSQL (DB)
- Redis (in-memory DB & caching)

## Current Viva Translate Stack

- GitHub
    - Markdown (docs)
    - GitHub pages from markdown (GettingStarted and HowTo's)
    - Collaborative code review
    - Version control
    - SourceTree? (git GUI)
- Jira
    - Issue/feature tracking
- Visual Studio Code (IDE)
- GitHub Actions (CI-CD)
- ESLint (js linting)
- None -> flake8 (py linting)
- None (styleguide, code health)
- Testing
    - None -> Jest (javascript)
    - None -> doctests+pytest(python)
- Docker/Docker-Compose (container management)
- EC2+EBS (VMs) -> Terraform
- Lambda -> Terraform (cluster management)
- Laptop Docker (qa/dev environments)
- Amazon EC2 + S3 (staging servers)
- Databases
    - Redis (in-memory caching)
    - Redis (logging) -> AWS DynamoDB
    - CSVs -> managed PostgreSQL (persistent)
    - none -> Django (ORM)
- FastAPI -> FastAPI + Django (admin interface)
    - [tutorial](https://www.stavros.io/posts/fastapi-with-django/)

## Security Vulnerabilities

1. No SSL/HTTPS certs -> TLDs+Certbot (John working it)
2. No FastAPI authentication (tokens/keys) -> [tutorial](https://testdriven.io/blog/fastapi-jwt-auth/)
3. External API keys in code on GitHub -> `env` vars
4. Passwords shared on Slack -> BitWarden
5. admin AWS permissions for all devs
6. no load balancer (DDOS) -> Terraform EKS
7. no CDN (DDOS) -> CloudFlare

### Fixed

1. Shared AWS root account

## Recommendations

- Lambda/EBS/EC2 -> Terraform
    - [tutorial](https://www.bogotobogo.com/DevOps/Docker/Docker_Kubernetes_Terraform_EKS.php)
    - performance
    - cost
    - scalability
    - stable API
    - customization
    - security (DDOS etc)
- None -> flake8 (py linting)
- Testing 
    - None -> Jest (javascript)
        - [tutorial](https://medium.com/2359media/5-techniques-for-frontend-testing-with-jest-and-enzyme-8e926b3c92f8)
        - most popular and oldest testing framework, plays nice with React
    - None -> doctests+pytest(python)
- Databases
    - Redis -> DynamoDB (logging)
    - None -> managed PostgreSQL (persistent)
- FastAPI -> FastAPI + Django (admin interface)
    - [tutorial](https://www.stavros.io/posts/fastapi-with-django/)
    - flexibility
    - developer UX
    - security
- None -> nginx (facade web server for prod)
    - security
    - performance
    - caching
    - separation of concerns (static docs vs dynamic API)
- None -> LetsEncrypt certbot (SSL certs)
    - familiarity
    - free
    

## Possible Alternatives

- GitHub Actions -> CircleCI
    - [tutorial](https://circleci.com/blog/learn-iac-part1/)
    - more compatible with AWS
    - standard API
    - stable UX



