# Prosocial AI Project Ideas

For a database of prosocial project ideas check out this yaml file: 

[prosocial-ai-project-ideas-plan-your-project.yml](prosocial-ai-project-ideas-plan-your-project.yml)


To exercise your git, yaml, database, and ideation skills you should add at least one idea to this database the next time you think of it:

1. On gitlab.com/tangibleai/team: fork this repository on GitLab.com
2. Clone your fork from `https://gitlab.com/{YOUR_ACCOUNT_NAME}/team` or `git@gitlab.com:{YOUR_ACCOUNT}/team` to your PC
3. Edit this file using sublime, nano, or you favorite IDE/text-editor
4. Commit and push your changes to your fork of this repo
5. On gitlab.com/tangibleai/team: create a "merge request" from your repo to ours
6. Keep that yaml file available in the left hand file browser of your IDEA so you can edit it whenever you have an idea
