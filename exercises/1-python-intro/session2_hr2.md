# Improv Coding

## Problem/Question

Split a string based on capitalized letters.

## Copypasta from iPython

```python
In [162]: s = 'Hello world Goodbye world'

In [163]: from string import letters
---------------------------------------------------------------------------
ImportError                               Traceback (most recent call last)
<ipython-input-163-743a7a9438d9> in <module>
----> 1 from string import letters

ImportError: cannot import name 'letters' from 'string' (/home/hobs/anaconda3/envs/dsc/lib/python3.7/string.py)

In [164]: import string

In [165]: string.ascii_letters
Out[165]: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

In [166]: string.ascii_letters.lower()
Out[166]: 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'

In [167]: string.ascii_letters.upper()
Out[167]: 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'

In [168]: set(string.ascii_letters.upper())
Out[168]:
{'A',
 'B',
 'C',
 'D',
 'E',
 'F',
 'G',
 'H',
 'I',
 'J',
 'K',
 'L',
 'M',
 'N',
 'O',
 'P',
 'Q',
 'R',
 'S',
 'T',
 'U',
 'V',
 'W',
 'X',
 'Y',
 'Z'}

In [169]: uppers = set(string.ascii_letters.upper())

In [170]: [c for c in s]
Out[170]:
['H',
 'e',
 'l',
 'l',
 'o',
 ' ',
 'w',
 'o',
 'r',
 'l',
 'd',
 ' ',
 'G',
 'o',
 'o',
 'd',
 'b',
 'y',
 'e',
 ' ',
 'w',
 'o',
 'r',
 'l',
 'd']

In [171]: [c if c not in uppers else c for c in s]
Out[171]:
['H',
 'e',
 'l',
 'l',
 'o',
 ' ',
 'w',
 'o',
 'r',
 'l',
 'd',
 ' ',
 'G',
 'o',
 'o',
 'd',
 'b',
 'y',
 'e',
 ' ',
 'w',
 'o',
 'r',
 'l',
 'd']

In [172]: [c if c not in uppers else ',' + c for c in s]
Out[172]:
[',H',
 'e',
 'l',
 'l',
 'o',
 ' ',
 'w',
 'o',
 'r',
 'l',
 'd',
 ' ',
 ',G',
 'o',
 'o',
 'd',
 'b',
 'y',
 'e',
 ' ',
 'w',
 'o',
 'r',
 'l',
 'd']

In [173]: ''.join([c if c not in uppers else ',' + c for c in s])
Out[173]: ',Hello world ,Goodbye world'

In [174]: ''.join([c if c not in uppers else ',' + c for c in s]).split(',')
Out[174]: ['', 'Hello world ', 'Goodbye world']

In [175]: ''.join([c if c not in uppers else ',' + c for c in s]).strip().strip(',').split(',')
Out[175]: ['Hello world ', 'Goodbye world']

In [176]: 'lower' in dir('ect')
```
