# Assignment: Fake News, Real NLP
​
Imagine your employer has developed a dataset of News and labeled it as real or fake.
They have asked our interns to design a model to predict the truthfulness of
an article.

## Learning Goals:
- [ ] Dive into a dataset and review its quality
- [ ] Practice ETL, EDA, Feature Engineering and Model Training.
- [ ] Pick a single model and feature that you think will be well suited to the problem.
- [ ] Solve an NLP-style problem using a toolset of your selection.
- [ ] Write a few words on how your plan met or did not meet your expectations.
- [ ] Write a abstract and summary for your model explaining why you chose it and its metrics.

### Deliverable: 
**Create a Jupyter Notebook (.ipynb) or Python script (.py) that includes your python code you used to answer the questions below.**
### Duration: 7 Days
### Dataset: https://www.kaggle.com/clmentbisaillon/fake-and-real-news-dataset/download
### Objective: Create a table or your results and explain what worked and what didn't.
​
## Some questions to help you get started:
​
### ETL (Extraction, Transform, Load)
- How is our dataset organized? Anything unusual about it?
- If you were collecting the data yourself, how would you have organized it?
- How was the data collected and labeled as fake or not-fake? As a scientist is there a better way to collect and label data?
- Is there any background on the dataset that could be useful?

### Feature Engineering
- What is your target variable (column)?
- What features about news articles were colleced for this dataset?
- What are some numerical variables (scores, counts) that you might be able to extract from the natural language strings in this dataset?
- Are there any additional features (data about each news article) that you would have liked to have?
- What are the easiest features you can extract from any text sting?
- Can you think of some more difficult features to extract from text strings?
- Which features will you start with to train your first model?

### Train your first model
- Is this a classification or a regression problem?
- What is your "go-to" model for this kind of problem?
- Train your model on only one feature variable.
- What is your prediction accuracy for that one feature? 

### EDA (Exploratory Data Analysis)
- Plot some histograms and scatter plots of the numerical data you extracted from the dataset to learn more about the problem.
- What are some strengths and weaknesses of your current dataset?
- Are a majority of our data fields (columns) one kind of data? Is this a good or bad thing?

### Hyperparameter tuning
- Create a dictionary containing your results for the previous experiment (model), for example: `{'score': model.score(X,y), 'description': 'univariate logistic regression'}`}
- Train more models by adding new features (columns) to your features (your `X` variable)? Always start with the simplest features you can come up with. Simple features like the count of capitalized or lowercase letters or punctuation can be surprisingly powerful.
- Advanced: Try different model "classes" from Scikit-Learn, and try different hyper parameter values like TfidfVectorizer(min_df=?, max_df=?, ngram_range=(?, ?), analyzer=?)
- After you've trained two or more models, put your results dictionaries into a table: `pd.DataFrame([results_dict_model1, results_dict_model_2])`
- Which model best predicts the target variable (`isfake`)?
- What are the coefficients for each of your features in your best model?
- What can you do differently to detect *overfitting*?
- Advanced: Use spacy or hugging face transformers to extract some natural language embeddings or vector encodings of the natural language fields.
- Advanced: What can you do differently to **prevent** overfitting?
- Advanced: What is the fewest number of features (including embedding vector dimensions) that you need to achieve nearly 99% accuracy on your test set?

### Big picture
- What are Python libraries (packages) did you use?
- Do we need to use advanced NLP to solve this problem?
- Whow did you creat table of your experiments and resulst (Hyperparameter table)?
- What was the biggest challenge in this assignment?

If you feel lost feel free to reach out on Slack.
