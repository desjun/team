# Gitpod

If you can't install (git, bash, and python) on your machine [Gitpod](gitpod.io) will save you.

[!gitpod-screenshot.png](gitpod-screenshot.png)

It gives you a full development environment and a full-featured IDE ([VSCodium](https://vscodium.com/)) you can use for any project.
And the nice thing is, you can collaborate with others by sharing
 or can't create a proper linux-based development environment, then you may be forced to use a browser-based development environment.

**[Gitpod](gitpod.io)** is an open source platform you can get your work done on without lock-in.


You can sketch out your latest ideas in code and share it with others using these platforms:

- [codiad](codiad.com) ([source code](https://github.com/Codiad/Codiad))
- [BrowXY](browxy.com)
- Glitch
- Github codespaces
- Google Colab
- Google  
