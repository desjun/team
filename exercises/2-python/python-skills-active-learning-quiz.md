# Python Programming Skills Checklist (with quiz questions):

## Python related skills from the full [skills checklist]()

### Basics
* [ ] getting help: ?, ??, [TAB], `dir()`, `vars()`, syntax-highlighting
* [ ] getting help: `help`, `print()`, `.__doc__`
* [ ] scalar numerical data types: `int`, `float`
* [ ] sequence container types: `str`, `bytes`, `list`, `tuple`
* [ ] sets: `set`, `.intersection`, `.__add__`, `.__subtract__`
* [ ] dicts: `dict`, `dict(enumerate(zip()))`, `dict.get()` `[k for k in dict()]`
* [ ] `iter(c).next()`, `type(c)` vs `type(c[0])`
* [ ] mapping: `dict`, `Counter`

### Intermediate
* [ ] reading Tracebacks and error messages
* [ ] importing python packages: `this`, `os`, `sys`
* [ ] style: var names (never reserved words/builtins!), zen, DRY, relative paths, urls 
* [ ] `__builtins__`: `vars(__builtins__)`
* [ ] classes: class, methods, attributes, `__init__`, `__dict__`, isattr, "everything is an object"
* [ ] containers: `list, dict, set, len`
* [ ] workflow: "reset & run all", side-effects, idempotence, persist your experiment log 
* [ ] install & import python packages: `Pandas`, `tqdm`
* [ ] `DataFrame`, `Series`,`array`
* [ ] conditional expressions: `if`, `else`, `>`, `<`, `==`, `!=`, `not`, `in` (vectorized)

### Files
* [ ] python path manipulation: `pathlib`, this, os, sys, sys.path_append() 
* [ ] navigate directories: `cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`
* [ ] paths: PYTHONPATH, which python, `sys.path_append()`
* [ ] files: `with`, `open`, `write`
* [ ] manipulate files with bash: `!`, `mkdir ~/code`, `mv`, `cp`, `touch`, `!cat`
* [ ] files: `with`, `open`, `write`, `read`, `readlines`

### Data
* [ ] loops: `for`, `while`, `enumerate`
* [ ] working with iterables: `zip`, `zip*`, `list()` coercion
* [ ] functions: `args`, `kwargs`, `def`, `return`
* [ ] scalar numerical data types: int, float
* [ ] sequence data types: str, bytes, list, tuple
* [ ] mapping: dict, Counter
* [ ] type coercion: `list(str)`, `dict([(1,2)]`, `dict(zip(range(4), 'abcd'))`


## Python Programming Checklist (Condensed)

* [ ] importing python packages: `this`, `os`, `sys`, `sys.path_append()`
* [ ] finding, installing, importing python packages: `sklearn`, `Pandas`, `tqdm`
* [ ] reading Tracebacks and error messages
* [ ] conditional expressions: `if`, `else`, `>`, `<`, `==`, `!=`, `not`, `in` 
* [ ] loops: `for`, `while`, `enumerate`, `zip`, `zip*`, `tqdm`
* [ ] functions: args, kwargs, `def`, `return`
* [ ] classes: `class`, methods, attributes, `__init__`, "everything is an object"
* [ ] scalar numerical data types: `int`, `float`
* [ ] sequence data types: `str`, `bytes`, `list`, `tuple`
* [ ] mapping: `dict`, `Counter`
* [ ] type coercion: `list(str)`, `dict([(1,2)]`, `dict(zip(range(4), 'abcd'))`
* [ ] sets: `set`
* [ ] getting help: `help()`, `?`, `??`, `[TAB] completion`, `vars()`, `dir()`, `type()`, `print()`, `.__doc__`)
* [ ] files: `with`, `open`, `write` 
* [ ] manipulate files with bash: `!`, `pwd`, `mkdir ~/code`, `mv`, `cp`, `touch`
* [ ] navigate directories: `cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`

## Active learning with doctests (or quizzes)

These are called doctests. The question is the python code the line prefixed with `>>>`. The correct answer is the value you would put on the line after the python code based on what that expression would output.

Students should learn how to come up with odd exploratory examples like these using other built-in types. 
Try to make python do unexpectedly interesting things.

```python
>>> dict(enumerate('Hello'))[1]
'e'
```

```python
>>> dict(enumerate('Hello'))[:-1]
TypeError: unhashable type: 'slice'
```

```python
>>> set('Oh Hello'.lower()) - set('Ohio ')
{'e', 'l'}
```

```python
>>> dict(Counter(list('ABBA'))
Counter({'A': 2, 'B': 2})
```

```python
>>> dict(zip(*['RD', '2'*2]))
{'R': '2', 'D': '2'}
```

```python
>>> pwd
>>> set(Path('.').absolute().__str__()) - set(_)
set()
```

```python
>>> def f(x):
...     while x > 2:
...         x -= 2
...     return x
>>> f(128)
2
```

```python
>>> 'lower' in dir('ect')
True
```

```python
>>> 'dir' in list('direct')
False
```

```python
>>> [1, 2, 3] * 2
[1, 2, 3, 1, 2, 3]
```

```python
>>> np.array([1, 2, 3]) * 2
[2, 4, 6]
```

```python
>>> pd.Series([1, 2, 3]) - 1
0    0
1    1
2    2
dtype: int64
```

```python
>>> pd.Series([1, 2, 3]) + pd.Series([1, 2, 3], index=[1, 2, 3])
0    NaN
1    3.0
2    5.0
3    NaN
```
