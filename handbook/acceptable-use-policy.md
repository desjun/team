## Purpose

This policy specifies requirements related to the use of computing resources and data assets by Tangible AI team members so as to protect our customers, team members, contractors, company, and other partners from harm caused by both deliberate and inadvertent misuse. Our intention in publishing this policy is to outline information security guidelines intended to protect GitLab assets, not to impose restrictions.

It is the responsibility of every member of our Community to interact with Tangible AI resources and data in a secure manner and to that end we provide the following acceptable use standards related to computing resources, company and customer data, mobile and tablet devices, and removable and external media storage devices.

## Scope

This policy applies to all GitLab team-members, contractors, advisors, and contracted parties interacting with GitLab computing resources and accessing company or customer data.

## Roles & Responsibilities:

| Role  | Responsibility | 
|-----------|-----------|
| Tangible AI Team Members | Responsible for following the requirements in this procedure |
| Security Team| Responsible for implementing and executing this procedure | 

## Procedure

### Acceptable Use and Security Requirements of Computing Resources at GitLab

GitLab-managed assets are provided to conduct GitLab business with consideration given for limited personal use. Our company uses global electronic communications and resources as routine parts of our business activities. It is essential that electronic resources used to perform company business are protected to ensure that these resources are accessible for business purposes and operated in a cost-effective manner, that our company’s reputation is protected, and that we minimize the potential for legal risk.

Those receiving GitLab-provided assets are responsible for exercising good judgment when accessing GitLab-managed data.

### Security and Proprietary Information

All GitLab data is categorized and must be handled in accordance with the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html). All computing assets and 3rd party services that are used by Tangible AI, must comply with the applicable standards.


### Freeware, Browser Extensions, Add-ons and Plugins
[Freeware, browser extensions, add-ons and plugins](https://about.gitlab.com/handbook/tools-and-tips/other-apps/#browser-extensions) can pose a risk to Tangible AI as they may contain viruses, spyware or adware. The use of freeware could result in the loss of Tangible AI data and the inability to protect the data in accordance with Tangible AI security and privacy requirements. Not all freeware contains malware, but team members should carefully consider the terms of service and [types of data](/handbook/engineering/security/data-classification-standard.html) that will be shared before installing anything on your computer.    

### Unacceptable Use

Team members and contractors may **not** use company-managed resources for activities that are illegal or prohibited under applicable law, no matter the circumstances.

### Unacceptable System and Network Activities

Prohibited system and network activities include, but are not limited to, the following:

- Violations of the rights of any person or company protected by copyright, trade secret, patent or other intellectual property, or similar laws or regulations.
- Unauthorized copying, distribution, or use of copyrighted material.
- Exporting software, technical information, encryption software, or technology in violation of international or national export control laws.
- Intentional introduction of malicious programs into GitLab networks or any GitLab-managed computing device.
- Intentional misuse of any GitLab-managed computing device or GitLab networks (e.g. for cryptocurrency mining, botnet control, etc.).
- Sharing your credentials for any GitLab-managed computer or 3rd party service that GitLab uses with others, or allowing use of your account or a GitLab-managed computer by others. This prohibition does not apply to single-sign-on or similar technologies, the use of which is approved.
- Using a GitLab computing asset to procure or transmit material that is in violation of sexual harassment policies or that creates a hostile workplace.
- Making fraudulent offers of products, items, or services originating from any GitLab account.
- Intentionally accessing data or logging into a computer or account that the team member or contractor is not authorized to access, or disrupting network communication, computer processing, or access.
- Executing any form of network monitoring that intercepts data not intended for the team member’s or contractor's computer, except when troubleshooting networking issues for the benefit of GitLab.
- Circumventing user authentication or security of any computer host, network, or account used by GitLab.
- Tunneling between network segments or security zones (e.g., `gprd`, `gstg`, `ops`, `ci`), except when troubleshooting issues for the benefit of GitLab.
- Given the potential sensitivity of the data contained in screenshot images, the use of tools that capture and share screenshots to hosted sites online is prohibited without the explicit approval of the Security and Legal Departments.  Screenshots should be stored locally or within Google drive folders associated with your GitLab.com account. Access to these drives and files should be managed in accordance with our [Access Control policy](/handbook/engineering/security/#access-management-process), and handled according to our [Data Classification Policy](/handbook/engineering/security/data-classification-standard.html). Tools such as [Lightshot](https://app.prntscr.com/en/index.html), where upload functionality cannot be disabled and could result in inadvertant uploads, should not be used.

### Unacceptable Email and Communications Activities

Forwarding of confidential business emails or documents to personal external email addresses is prohibited.

> Note: GitLab may retrieve messages from archives and servers without prior notice if GitLab has sufficient reason to do so. If deemed necessary, this investigation will be conducted with the knowledge and approval of the Security, People Business Partners, and Legal Departments.

When utilizing social media think about the effects of statements that you make. Keep in mind that these transmissions are permanent and easily transferable, and can affect our company’s reputation and relationships with team members and customers. When using social media tools like blogs, Facebook, Twitter or wikis, ensure that you do not make comments on behalf of GitLab without proper authorization. Also, you must not disclose our company’s confidential or proprietary information about our business, our suppliers, or our customers.

### Return of GitLab-Owned Assets

All Tangible AI-owned computing resources, as well all records including company and client data, must be returned upon separation from the company.

### Bring-Your-Own-Device
Some team members and contractors utilize their personal devices in order to complete task on behalf of the company. Doing so poses additional risk to the security of company's data and information. All employees, contractors and collaborators that work with Tangible AI data and information on their personal computers are required to abide by the following procedures: 

1. All personal computers storing or processing Tangible AI-related data should have a strong password as specified by Tangible AI password policy. 
1. All Tangible AI related data stored on a personal computer should be stored in a dedicated folder labeled Tangible AI. The above folder with all its content should be removed 
1. Tangible AI reserves the right to demand in-writing confirmation of deletion of all company and client data.

### Personal Mobile Phone and Tablet Usage

All personal mobile computing devices used to access Tangible AI  data, including but not limited to email, must be passcode-enabled. 2FA will be enforced by the Security team for all employee and contractor Google Workspace accounts. Mobile computing best practices dictate that these devices should be running the latest version of the operating system available, and all new patches applied. For assistance with determining the suitability of your mobile device, please contact the Security Team.

### Mobile Messaging

All GitLab-related conversations need to take place in Slack. It is strongly recommended that the official Slack application, or Slack web application, are used for mobile messaging. Downloads are available for [iOS](https://itunes.apple.com/app/slack-app/id618783545?ls=1&mt=8) and [Android](https://play.google.com/store/apps/details?id=com.Slack). While it may be more convenient to use an integrated chat application that puts all of your conversations in one place, the use of these applications can unintentionally lead to work-related conversations crossing platforms, or being sent to external contacts. The use of Slack for all work communications assists with our security and compliance efforts. For example, in the case of an incident response issue, it may be necessary to review a conversation to understand the order in which events occurred, or to provide evidence that the chain of custody has been maintained for forensic evidence during a handoff.

For [video calls](/handbook/communication/#video-calls) we prefer Zoom. Zoom chats are an acceptable alternative to Slack when in a video call. If the conversation is interesting to others or may be needed for a retrospective, consider recording the call.


### Lost or Stolen Procedures
Please ensure that assets holding data in scope are not left unduly exposed, for example visible in the back seat of your car.

 Should a team member lose a device such as a  mobile phone, tablet, laptop, etc. that contains their credentials or other Tangible AI sensitive data, they should send an email to `security@tangibleai.com` right away. 

### Policy Compliance

Compliance with this policy will be verified through various methods, including but not limited to, automated reporting, audits, and feedback to the policy owner.

Any team member or contractor found to be in violation of this policy may be subject to disciplinary action, up to and including termination of employment, or contractual agreement.